package br.ucsal.testequalidade20162.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

	public static LocalDateTime parse(String date) {
		return LocalDateTime.parse(date, formatter);
	}

	public static String format(LocalDateTime date) {
		return formatter.format(date);
	}

}
